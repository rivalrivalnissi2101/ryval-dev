const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const QuizReportViolationsSchema = Schema({
    user_id : {
        type: String,
        required: false,
    },
    origin_quiz: {
        type: String,
        required: false,
    },
    quiz_id : {
        type: String, 
        required: false,
    },
    question_id : {
        type: String, 
        required: false,
    },
    report: {
        type: String, 
        required: false
    },
    detail_report: {
        type: String, 
        required: false
    },
    createdAt : {
        type: Date,
        default: Date.now()
    },
    updatedAt:{
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model('quiz_report_violations', QuizReportViolationsSchema)