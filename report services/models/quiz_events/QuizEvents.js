const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const QuizEventsSchema = Schema({
    is_template: {
        type: Boolean, 
        required: false
    },
    quiz_id: {
        type: String, 
        required: false
    },
    quiz_name: {
        type: String, 
        required: false
    },
    creator_id: {
        type: String,
        required: false
    },
    login_mode: {
        type: Boolean,
        required: false
    },
    quiz_code: {
        type: String, 
        required: false,
    },
    event_mode: {
        type: String,
        required: false
    },
    play_mode: {
        type: String,
        required: false
    },
    quiz_start: {
        type: Boolean,
        required: false,
        default: false
    },
    sound_true: {
        type: Number,
        required: false,
        default: 0,
    },
    sound_false: {
        type: Number, 
        required: false,
        default: 0,
    },
    createdAt : {
        type: Date,
        default: Date.now()
    },
    updatedAt:{
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model('quiz_events', QuizEventsSchema)