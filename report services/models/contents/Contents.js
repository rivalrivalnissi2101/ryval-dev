const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ContentSchema = Schema({
    content_name: {
        type: String, 
        required: false
    },
    category_id: {
        type: String,
        required: false
    },
    creator_id: {
        type: String, 
        required: false,
    },
    content_type: {
        type: String,
        required: false,
    },
    image_thumbnail: {
        type: String, 
        required: false,
        default: ""
    },
    image_array: {
        type: String, 
        required: false,
    },
    job_id: {
        type: String, 
        required: false, 
    },
    audio_array: {
        type: String, 
        required: false,
    },
    music_array: {
        type: String, 
        required: false,
    },
    content_file: {
        type: String, 
        required: false
    },
    generate_tta_final: {
        type: Boolean, 
        required: false,
        default: true
    },
    public_mode: {
        type: Boolean,
        required: false, 
        default: false
    },
    volume_music: {
        type: Number, 
        required: false, 
        default: 0.35
    }, 
    volume_audio: {
        type: Number, 
        required: false, 
        default: 1
    },
    rating: {
        type: Number, 
        required: false,
    },
    deleted: {
        type: Boolean,
        required: false,
        default: false
    },
    is_draft: {
        type:Boolean, 
        required: false,
        default: false
    },
    createdAt : {
        type: Date,
        default: Date.now()
    },
    updatedAt:{
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model('contents', ContentSchema)