const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserImageLibrarySchema = Schema({
    user_id: {
        type: String, 
        required: false
    },
    image_url: {
        type: String,
        required: false
    },
    filename: {
        type: String, 
        required: false,
    },
    deleted: {
        type: Boolean,
        required: false, 
        default: false
    },
    createdAt : {
        type: Date,
        default: Date.now()
    },
    updatedAt:{
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model('user_image_library', UserImageLibrarySchema)