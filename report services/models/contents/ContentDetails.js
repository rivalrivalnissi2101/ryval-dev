const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ContentDetailsSchema = Schema({
    content_id: {
        type: String, 
        required: false
    },
    script: {
        type: String, 
        required: false,
    },
    content_file: {
        type: String, 
        required: false,
    },
    createdAt : {
        type: Date,
        default: Date.now()
    },
    updatedAt:{
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model('content_details', ContentDetailsSchema)