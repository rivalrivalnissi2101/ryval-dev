const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const QuizResultsDetailsSchema = Schema({
    quiz_results_id: {
        type: String, 
        required: false
    },
    question_id: {
        type:String,
        required: false
    },
    answer: {
        type: String,
        required:false
    },
    answer_result: {
        type: Boolean,
        required: false,
    },
    score: {
        type: Number, 
        required: false
    },
    answer_match_1 : {
        type: String, 
        required: false
    },
    answer_result_match_1: {
        type: Boolean,
        required: false
    },
    score_match_1: {
        type: Number, 
        required: false
    },
    answer_match_2 : {
        type: String, 
        required: false
    },
    answer_result_match_2: {
        type: Boolean,
        required: false
    },
    score_match_2: {
        type: Number, 
        required: false
    },
    answer_match_3 : {
        type: String, 
        required: false
    },
    answer_result_match_3: {
        type: Boolean,
        required: false
    },
    score_match_3: {
        type: Number, 
        required: false
    },
    answer_match_4 : {
        type: String, 
        required: false
    },
    answer_result_match_4: {
        type: Boolean,
        required: false
    },
    score_match_4: {
        type: Number, 
        required: false
    },
    total_score_match : {
        type: Number, 
        required: false
    },
    createdAt : {
        type: Date,
        default: Date.now()
    },
    updatedAt:{
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model('quiz_results_details', QuizResultsDetailsSchema)