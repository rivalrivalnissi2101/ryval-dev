const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const QuizEventsTeamsSchema = Schema({
    quiz_events_id: {
        type: String, 
        required: false
    },
    team_name: {
        type: String, 
        required: false
    },
    createdAt : {
        type: Date,
        default: Date.now()
    },
    updatedAt:{
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model('quiz_events_teams', QuizEventsTeamsSchema)