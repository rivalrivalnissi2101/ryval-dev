const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const QuizResultsSchema = Schema({
    quiz_events_id: {
        type: String, 
        required: false
    },
    quiz_id: {
        type: String,
        required: false
    },
    category_id: {
        type: String, 
        required: false
    },
    origin_quiz: {
        type: String, 
        required: false
    },
    events_teams_id : {
        type: String, 
        required: false
    },
    events_duels_id :{
        type: String, 
        required: false
    },
    user_id: {
        type: String, 
        required: false
    },
    user_name: {
        type: String, 
        required: false
    },
    email: {
        type: String, 
        required: false
    },
    score: {
        type: Number, 
        required: false,
        default: 0
    },
    quiz_events_status: {
        type: Number,
        required: false,
        default: 0
    },  
    data_detail : {
        type: String, 
        required: false,
    },
    createdAt : {
        type: Date,
        default: Date.now()
    },
    updatedAt:{
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model('quiz_results', QuizResultsSchema)