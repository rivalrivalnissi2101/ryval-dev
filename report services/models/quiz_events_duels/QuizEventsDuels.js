const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const QuizEventsDuelsSchema = Schema({
    quiz_events_id: {
        type: String, 
        required: false
    },
    duel_name: {
        type: String, 
        required: false
    },
    duel_number: {
        type: Number,
        required: false
    },
    createdAt : {
        type: Date,
        default: Date.now()
    },
    updatedAt:{
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model('quiz_events_duels', QuizEventsDuelsSchema)