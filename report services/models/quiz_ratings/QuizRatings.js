const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const QuizRatingsSchema = Schema({
    user_id : {
        type: String,
        required: false,
    },
    origin_quiz: {
        type: String,
        required: false,
    },
    quiz_id : {
        type: String, 
        required: false,
    },
    createdAt : {
        type: Date,
        default: Date.now()
    },
    updatedAt:{
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model('quiz_ratings', QuizRatingsSchema)